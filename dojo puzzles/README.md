# Produto Escalar de Vetores

> http://dojopuzzles.com/problemas/exibe/produto-escalar-de-vetores/

Definimos dois vetores A e B de dimensão n em termos de componentes como:

A = (a1, a2, a3, ..., an) e B = (b1, b2, b3, ..., bn)

O produto escalar entre esses vetores é descrito como:

A . B = a1 * b1 + a2 * b2 + a3 * b3 + ... + an * bn

Desenvolva um programa que, dado dois vetores de dimensão n, retorne o produto escalar entre eles.

## Como rodar

> Você precisa ter o python 3.7 ou superior para rodar os resultados

```sh
$ python "/pasta/do/repositorio/dojo puzzles/produto escalar de vetores"
```

## Resolução


```python
lenght = range(7) # 0 - N
A = [i+1 for i in lenght]
B = [i+1 for i in lenght]

AB = 0
for item in lenght:
    AB += A[item]*B[item]

print(AB)
```


# Anagramas

> http://dojopuzzles.com/problemas/exibe/anagramas/

Escreva um programa que gere todos os anagramas potenciais de uma string.

Por exmplo, os anagramas potenciais de "biro" são:

biro bior brio broi boir bori
ibro ibor irbo irob iobr iorb
rbio rboi ribo riob roib robi
obir obri oibr oirb orbi orib

## Como rodar

> Você precisa ter o python 3.7 ou superior para rodar os resultados

```sh
$ python "/pasta/do/repositorio/dojo puzzles/anagramas" <string>
```


## Resolução

```python
from random import randint
def get_random_letter(word:str) -> str:
    p = [randint(0, i)
         for i in range(randint(0, len(word)*10))]

    possibility = 0
    for i in p:
        possibility += i

    return word[
        possibility%len(word)
    ]

def fatorial(num:int) -> int:
    return 1 if num == 0 else num*fatorial(num-1)

def calculate_permutation(num:int, rep:list) -> int:
    'calculate permutation with the formule.'

    res = 1
    for n in rep:
        res *= fatorial(n)
    res = fatorial(num)//res

    return res


def get_permutation_limit(word:str) -> int:
    'calculate permutation of word.'

    rep, history = [], []
    for letter in word:
        total = word.count(letter)
        if (letter not in history) and (total):
            rep.append(total)

            history.append(letter)

    return calculate_permutation(len(word), rep)


def get_permutation(word:str, force_list:bool = False) -> str:
    'returns the possible permutation of a word.'

    history = []
    while len(history) < get_permutation_limit(word):
        new_word = ''
        letter_history = {}

        while len(new_word) < len(word):
            letter = get_random_letter(word)
            if letter in letter_history:
                letter_history[letter] += 1
            else:
                letter_history[letter] = 1
                new_word += letter
                continue

            if letter_history[letter] <= word.count(letter):
                new_word += letter

        if new_word not in history:
            history.append(new_word)
            yield new_word


if __name__ == '__main__':
    from sys import argv as args, stderr as e
    try:
        for anagram in get_permutation(args[1]):
            print(anagram)
    except IndexError:
        print('Usage: python "anagramas" <word>', file=e)
        exit(1)
```


# Matriz Espiral

> http://dojopuzzles.com/problemas/exibe/matriz-espiral/

Dado um número de colunas e um número de linhas, retornar uma matriz em espiral de fora para dentro no sentido horário.

Variação do problema: retorne a espiral no sentido anti-horário.

Este problema é mais fácil de ser compreendido através de exemplos:

Entrada: `3 4`

Saída:

```
 1  2 3
10 11 4
 9 12 5
 8  7 6
```


Entrada: `5 6`

Saída:

```
 1  2  3  4  5
18 19 20 21  6
17 28 29 22  7
16 27 30 23  8
15 26 25 24  9
14 13 12 11 10
```

## Como rodar

> Você precisa ter o python 3.7 ou superior para rodar os resultados

```sh
$ python "/pasta/do/repositorio/dojo puzzles/matriz espiral" <qnt de linhas> <qnt de colunas>
```

## Resolução

```python
def next_value(number:int, limit:int) -> int:
    return number + 1 if number < limit else limit
def prev_value(number:int, limit:int) -> int:
    return number - 1 if number > limit else limit

def get_matriz_espiral(lines:str, columns:str) -> list:

    lines = range(lines)
    columns = range(columns)

    matriz = [[
        None for c in columns
    ] for l in lines]

    lenght = (columns[-1]+1)*(lines[-1]+1)

    cursor, l, c = 1, 0, 0
    next_c, next_l = lambda: c, lambda: l
    while cursor <= lenght:
        # Changing into sides

        if c == columns[0]:
            next_c = lambda: next_value(c, columns[-1])
        if c == columns[-1]:
            next_l = lambda: next_value(l, lines[-1])
        if l == lines[-1]:
            next_c = lambda: prev_value(c, columns[0])

        if l > lines[1] and c == columns[0]:
            next_l = lambda: prev_value(l, lines[0])
            next_c = lambda: c
        elif c == columns[0]:
            next_l = lambda: l

        # Changing into middle

        if l in lines[1:-1] and c in columns[1:-1]:
            if matriz[l][c+1]:
                if not matriz[l+1][c]:
                    next_l = lambda: next_value(l, lines[-1])
                    next_c = lambda: c
            if matriz[l+1][c]:
                if not matriz[l][c-1]:
                    next_l = lambda: l
                    next_c = lambda: prev_value(c, columns[0])

            if matriz[l][c-1]:
                if not matriz[l-1][c]:
                    next_l = lambda: prev_value(l, lines[0])
                    next_c = lambda: c
            if matriz[l-1][c]:
                if not matriz[l][c+1]:
                    next_l = lambda: l
                    next_c = lambda: next_value(c, columns[-1])


        matriz[l][c] = cursor
        c, l = next_c(), next_l()
        cursor = next_value(cursor, lenght+1)
    return matriz

if __name__ == '__main__':
    from sys import argv as args, stderr as e

    try:
        columns = int(args[1])
        lines = int(args[2])
    except IndexError:
        print('Usage: python "matriz espiral" <lenght> <height>', file=e)
        exit(1)

    matriz = get_matriz_espiral(lines, columns)

    sep = 0
    for l in lines:
        for c in columns:
            lenght = len(str(matriz[l][c]))
            if lenght > sep:
                sep = lenght

    for l in lines:
        for c in columns:
            print(
                ' '*(sep-len(str(matriz[l][c]))),
                matriz[l][c],
                end = ''
            )
        print()
```
