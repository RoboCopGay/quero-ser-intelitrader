from random import randint
def get_random_letter(word:str) -> str:
    p = [randint(0, i)
         for i in range(randint(0, len(word)*10))]

    possibility = 0
    for i in p:
        possibility += i

    return word[
        possibility%len(word)
    ]

def fatorial(num:int) -> int:
    return 1 if num == 0 else num*fatorial(num-1)

def calculate_permutation(num:int, rep:list) -> int:
    'calculate permutation with the formule.'

    res = 1
    for n in rep:
        res *= fatorial(n)
    res = fatorial(num)//res

    return res


def get_permutation_limit(word:str) -> int:
    'calculate permutation of word.'

    rep, history = [], []
    for letter in word:
        total = word.count(letter)
        if (letter not in history) and (total):
            rep.append(total)

            history.append(letter)

    return calculate_permutation(len(word), rep)


def get_permutation(word:str, force_list:bool = False) -> str:
    'returns the possible permutation of a word.'

    history = []
    while len(history) < get_permutation_limit(word):
        new_word = ''
        letter_history = {}

        while len(new_word) < len(word):
            letter = get_random_letter(word)
            if letter in letter_history:
                letter_history[letter] += 1
            else:
                letter_history[letter] = 1
                new_word += letter
                continue

            if letter_history[letter] <= word.count(letter):
                new_word += letter

        if new_word not in history:
            history.append(new_word)
            yield new_word


if __name__ == '__main__':
    from sys import argv as args, stderr as e
    try:
        for anagram in get_permutation(args[1]):
            print(anagram)
    except IndexError:
        print('Usage: python "anagramas" <word>', file=e)
        exit(1)
