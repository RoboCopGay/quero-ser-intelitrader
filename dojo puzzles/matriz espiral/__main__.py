def next_value(number:int, limit:int) -> int:
    return number + 1 if number < limit else limit
def prev_value(number:int, limit:int) -> int:
    return number - 1 if number > limit else limit

def get_matriz_espiral(lines:str, columns:str) -> list:

    lines = range(lines)
    columns = range(columns)

    matriz = [[
        None for c in columns
    ] for l in lines]

    lenght = (columns[-1]+1)*(lines[-1]+1)

    cursor, l, c = 1, 0, 0
    next_c, next_l = lambda: c, lambda: l
    while cursor <= lenght:
        # Changing into sides

        if c == columns[0]:
            next_c = lambda: next_value(c, columns[-1])
        if c == columns[-1]:
            next_l = lambda: next_value(l, lines[-1])
        if l == lines[-1]:
            next_c = lambda: prev_value(c, columns[0])

        if l > lines[1] and c == columns[0]:
            next_l = lambda: prev_value(l, lines[0])
            next_c = lambda: c
        elif c == columns[0]:
            next_l = lambda: l

        # Changing into middle

        if l in lines[1:-1] and c in columns[1:-1]:
            if matriz[l][c+1]:
                if not matriz[l+1][c]:
                    next_l = lambda: next_value(l, lines[-1])
                    next_c = lambda: c
            if matriz[l+1][c]:
                if not matriz[l][c-1]:
                    next_l = lambda: l
                    next_c = lambda: prev_value(c, columns[0])

            if matriz[l][c-1]:
                if not matriz[l-1][c]:
                    next_l = lambda: prev_value(l, lines[0])
                    next_c = lambda: c
            if matriz[l-1][c]:
                if not matriz[l][c+1]:
                    next_l = lambda: l
                    next_c = lambda: next_value(c, columns[-1])


        matriz[l][c] = cursor
        c, l = next_c(), next_l()
        cursor = next_value(cursor, lenght+1)
    return matriz

if __name__ == '__main__':
    from sys import argv as args, stderr as e

    try:
        columns = int(args[1])
        lines = int(args[2])
    except IndexError:
        print('Usage: python "matriz espiral" <lenght> <height>', file=e)
        exit(1)

    matriz = get_matriz_espiral(lines, columns)

    sep = 0
    for l in lines:
        for c in columns:
            lenght = len(str(matriz[l][c]))
            if lenght > sep:
                sep = lenght

    for l in lines:
        for c in columns:
            print(
                ' '*(sep-len(str(matriz[l][c]))),
                matriz[l][c],
                end = ''
            )
        print()
